//
//  StockSummaryViewController.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import UIKit
import RxCocoa
import RxSwift

class StockSummaryViewController: UIViewController {
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!

    // TODO: embed in stack view
    @IBOutlet private weak var keyStatisticStackView: UIStackView!

    @IBOutlet private weak var previousCloseStackView: UIStackView!
    @IBOutlet private weak var previousCloseLabel: UILabel!
    @IBOutlet private weak var openStackView: UIStackView!
    @IBOutlet private weak var openLabel: UILabel!
    @IBOutlet private weak var bidStackView: UIStackView!
    @IBOutlet private weak var bidLabel: UILabel!
    @IBOutlet private weak var askStackView: UIStackView!
    @IBOutlet private weak var askLabel: UILabel!
    @IBOutlet private weak var daysRangeStackView: UIStackView!
    @IBOutlet private weak var daysRangeLabel: UILabel!
    @IBOutlet private weak var marketCapStackView: UIStackView!
    @IBOutlet private weak var marketCapLabel: UILabel!
    @IBOutlet private weak var volumeStackView: UIStackView!
    @IBOutlet private weak var volumeLabel: UILabel!
    @IBOutlet private weak var averageVolumeStackView: UIStackView!
    @IBOutlet private weak var avgVolumeLabel: UILabel!
    @IBOutlet private weak var peRatioStackView: UIStackView!
    @IBOutlet private weak var peRatioLabel: UILabel!
    @IBOutlet private weak var dividentStackView: UIStackView!
    @IBOutlet private weak var dividendLabel: UILabel!

    private(set) var stockSummaryViewModel: StockSummaryViewModel!
    private var stockSummary = PublishSubject<StockSummaryModel>()
    private var marketSummary = BehaviorSubject<MarketSummaryModel?>(value: nil)
    private let disposeBag = DisposeBag()

    static func makeStockSummaryViewController(stockSummaryViewModel: StockSummaryViewModel) -> StockSummaryViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StockSummaryViewController") as! StockSummaryViewController
        vc.stockSummaryViewModel = stockSummaryViewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupControllerBinding()
        stockSummaryViewModel.getStockSummary()
        initialSetupUI()
    }

    private func setupControllerBinding() {
        stockSummaryViewModel
            .error
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] error in
                print(error)
                self?.showErrorAlert(message: error.element)
            }
            .disposed(by: disposeBag)

        stockSummaryViewModel
            .loading
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] value in
                print("Loading: \(value)")
                if value.element == true {
                    self?.activityIndicatorView.startAnimating()
                } else {
                    self?.activityIndicatorView.stopAnimating()
                }
            }
            .disposed(by: disposeBag)

        stockSummaryViewModel
            .stockSummary
            .observeOn(MainScheduler.instance)
            .bind(to: stockSummary)
            .disposed(by: disposeBag)

        stockSummaryViewModel
            .market
            .observeOn(MainScheduler.instance)
            .bind(to: marketSummary)
            .disposed(by: disposeBag)

        stockSummary
            .bind { [weak self] stockSummary in
                self?.setupUI(for: stockSummary)
            }
            .disposed(by: disposeBag)

        marketSummary
            .bind { [weak self] (market) in
                self?.title = market?.shortName
            }
            .disposed(by: disposeBag)
    }

    private func initialSetupUI() {
        activityIndicatorView.startAnimating()
        keyStatisticStackView.isHidden = true
    }

    private func setupUI(for stockSummary: StockSummaryModel) {
        keyStatisticStackView.isHidden = false

        previousCloseStackView.isHidden = stockSummary.summaryDetail?.previousClose?.longDetail == nil
        previousCloseLabel.text = stockSummary.summaryDetail?.previousClose?.longDetail

        openStackView.isHidden = stockSummary.summaryDetail?.open?.longDetail == nil
        openLabel.text = stockSummary.summaryDetail?.open?.longDetail

        bidStackView.isHidden = stockSummary.summaryDetail?.bid?.longDetail == nil
        bidLabel.text = stockSummary.summaryDetail?.bid?.longDetail

        askStackView.isHidden = stockSummary.summaryDetail?.ask?.longDetail == nil
        askLabel.text = stockSummary.summaryDetail?.ask?.longDetail

        if let low = stockSummary.summaryDetail?.dayLow?.longDetail,
           let high = stockSummary.summaryDetail?.dayHigh?.longDetail {
            daysRangeLabel.text = "[\(low) - \(high)]"
            daysRangeStackView.isHidden = false
        } else {
            daysRangeStackView.isHidden = true
        }

        marketCapStackView.isHidden = stockSummary.summaryDetail?.marketCap?.longDetail == nil
        marketCapLabel.text = stockSummary.summaryDetail?.marketCap?.longDetail

        volumeStackView.isHidden = stockSummary.summaryDetail?.volume?.longDetail == nil
        volumeLabel.text = stockSummary.summaryDetail?.volume?.longDetail

        averageVolumeStackView.isHidden = stockSummary.summaryDetail?.averageVolume?.longDetail == nil
        avgVolumeLabel.text = stockSummary.summaryDetail?.averageVolume?.longDetail

        peRatioStackView.isHidden = stockSummary.defaultKeyStatistics?.pegRatio.longDetail == nil
        peRatioLabel.text = stockSummary.defaultKeyStatistics?.pegRatio.longDetail

        dividentStackView.isHidden = stockSummary.defaultKeyStatistics?.lastDividendValue.longDetail == nil
        dividendLabel.text = stockSummary.defaultKeyStatistics?.lastDividendValue.longDetail
    }

    private func showErrorAlert(title: String? = "Error", message: String? = "Please try again later.") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { [weak self] _ in
            // TODO: temporary just pop to previous controller
            self?.navigationController?.popViewController(animated: true)
        }))
    }
}

extension GeneralFormatModel {
    var shortDetail: String? { fmt }

    var longDetail: String? { return longFmt ?? fmt }
}
