//
//  StockSummaryAssembly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import Foundation
import DITranquillity

class StockSummaryAssembly: DIPart {
    static func load(container: DIContainer) {
        container.register(StockSummaryViewController.makeStockSummaryViewController)
            .as(check: StockSummaryViewController.self, { $0 })

        container.register(StockSummaryViewModelImpl.init)
            .as(check: StockSummaryViewModel.self, { $0 })
    }

    static var container: DIContainer {
        let container = DIContainer()
        container.append([StockSummaryAssembly.self,
                          NetworkAssembly.self])
        return container
    }
}
