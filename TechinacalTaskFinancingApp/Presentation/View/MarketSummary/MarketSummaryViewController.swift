//
//  MarketSummaryViewController.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 22.12.2020.
//

import UIKit
import RxSwift
import RxCocoa

class MarketSummaryViewController: UIViewController {

    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableViewSearchBar: UISearchBar!

    private(set) var marketSummaryViewModel: MarketSummaryViewModel!
    private var marketSummaries = PublishSubject<[MarketSummaryModel]>()
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()

    static func makeMarketSummaryViewController(marketSummaryViewModel: MarketSummaryViewModel) -> MarketSummaryViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MarketSummaryViewController") as! MarketSummaryViewController
        vc.marketSummaryViewModel = marketSummaryViewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Market Summary"
        setupTableView()
        
        setupTableViewBinding()
        setupControllerBinding()

        marketSummaryViewModel.getMarketSummary()

        marketSummaryViewModel.startUpdatingMarketSummaryPeriodically()
    }

    private func setupTableView() {
        tableView.register(UINib(nibName: "MarketSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: MarketSummaryTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshMarketData), for: .valueChanged)
    }

    @objc
    private func refreshMarketData() {
        marketSummaryViewModel.getMarketSummary()
    }

    private func setupControllerBinding() {
        marketSummaryViewModel
            .error
            .observeOn(MainScheduler.instance)
            .subscribe { error in
                print(error)
            }
            .disposed(by: disposeBag)

        marketSummaryViewModel
            .loading
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] value in
                print("Loading: \(value)")
                if value.element == true {
                    self?.activityIndicatorView.startAnimating()
                } else {
                    self?.activityIndicatorView.stopAnimating()
                    self?.refreshControl.endRefreshing()
                }
            }
            .disposed(by: disposeBag)

        marketSummaryViewModel
            .marketSummaries
            .observeOn(MainScheduler.instance)
            .bind(to: marketSummaries)
            .disposed(by: disposeBag)
    }

    private func setupTableViewBinding() {
        marketSummaries
            .bind(to: tableView.rx.items(cellIdentifier: "MarketSummaryTableViewCell", cellType: MarketSummaryTableViewCell.self)) {  (row, market, cell) in
                cell.marketModel = market
            }
            .disposed(by: disposeBag)

        tableView.rx
            .modelSelected(MarketSummaryModel.self)
            .subscribe { [weak self] modelItem in
                guard let model = modelItem.element else { return }
                print(model)
                self?.navigate(model: model)
            }
            .disposed(by: disposeBag)

        tableViewSearchBar.rx
            .text
            .bind { [weak self] text in
                self?.marketSummaryViewModel.searchMarketSummary(for: text ?? "")
            }
            .disposed(by: disposeBag)
    }

    private func navigate(model: MarketSummaryModel) {
        let stockSummary = StockSummaryAssembly.container.resolve() as StockSummaryViewController
        stockSummary.stockSummaryViewModel.setMarketSummary(model)
        navigationController?.pushViewController(stockSummary, animated: true)
    }
}

extension MarketSummaryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UIDevice.current.hasNotch ? 34 : 0
    }
}
