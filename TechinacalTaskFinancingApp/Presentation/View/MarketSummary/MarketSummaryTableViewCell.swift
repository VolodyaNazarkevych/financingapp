//
//  MarketSummaryTableViewCell.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 29.12.2020.
//

import UIKit

class MarketSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var marketNameLabel: UILabel!
    @IBOutlet weak var marketPriceLabel: UILabel!
    @IBOutlet weak var growsPercentageLabel: UILabel!

    private let darkGreenColor = UIColor(red: 0, green: 100/255, blue: 0, alpha: 1)
    private let darkRedColor = UIColor(red: 100/255, green: 0, blue: 0, alpha: 1)

    override func awakeFromNib() {
        super.awakeFromNib()
        growsPercentageLabel.layer.cornerRadius = 8
        growsPercentageLabel.clipsToBounds = true
    }

    var marketModel: MarketSummaryModel? {
        didSet { setupUI() }
    }

    private func setupUI() {
        guard let marketModel = marketModel else { return }
        marketNameLabel.text = marketModel.shortName
        marketPriceLabel.text = marketModel.minCloseString

        growsPercentageLabel.text = String(format: "%.2f", marketModel.closeDifference)
        growsPercentageLabel.backgroundColor = marketModel.closeDifference == 0 ? .gray
            : marketModel.closeDifference > 0 ? darkGreenColor : darkRedColor
    }
}

extension Spark {
    var minClose: Double? { close?.min() }
}

extension MarketSummaryModel {

    var minCloseString: String? {
        guard let minCloseValue = spark.minClose else {
            return regularMarketPreviousClose.fmt
        }
        return "\(minCloseValue)"
    }

    var closeDifference: Double {
        guard let regularClose = regularMarketPreviousClose.raw,
              let minClose = spark.minClose else { return 0 }
        return minClose - regularClose
    }
}
