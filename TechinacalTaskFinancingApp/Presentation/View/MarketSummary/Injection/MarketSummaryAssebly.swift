//
//  MarketSummaryAssebly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import Foundation
import DITranquillity

class MarketSummaryAssembly: DIPart {
    static func load(container: DIContainer) {
        container.register(MarketSummaryViewController.makeMarketSummaryViewController)
            .as(check: MarketSummaryViewController.self, { $0 })
        container.register(MarketSummaryViewModelImpl.init)
            .as(check: MarketSummaryViewModel.self, { $0 })
    }

    static var container: DIContainer {
        let container = DIContainer()
        container.append([MarketSummaryAssembly.self,
                          NetworkAssembly.self])
        return container
    }
}
