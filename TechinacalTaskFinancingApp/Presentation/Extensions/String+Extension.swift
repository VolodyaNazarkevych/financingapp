//
//  String+Extension.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 03.01.2021.
//

import Foundation

extension String {
    func contains(find: String) -> Bool {
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool {
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
