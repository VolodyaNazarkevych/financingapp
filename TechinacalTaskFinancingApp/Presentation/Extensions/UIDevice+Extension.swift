//
//  UIDevice+Extension.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 03.01.2021.
//

import UIKit

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
