//
//  NetworkAssembly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import Foundation
import DITranquillity

class NetworkAssembly: DIPart {
    static func load(container: DIContainer) {
        container.register(YahooFinanceNetworkManager.init)
            .as(check: YahooFinanceNetworkManager.self, { $0 })
    }
}
