//
//  WindowAssembly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import UIKit
import DITranquillity

final class UIWindowAssembly: DIPart {
    static func load(container: DIContainer) {
        container.register { UIApplication.shared }.as(check: UIApplication.self, { $0 }).lifetime(.single)
        container.register { UIWindow(frame: UIScreen.main.bounds) }.as(check: UIWindow.self, { $0 }).lifetime(.single)
    }

    static var container: DIContainer {
        let container = DIContainer()
        container.append([UIWindowAssembly.self])

        #if DEBUG
        if !container.makeGraph().checkIsValid(checkGraphCycles: true) {
            fatalError("invalid graph")
        }
        #endif

        return container
    }
}
