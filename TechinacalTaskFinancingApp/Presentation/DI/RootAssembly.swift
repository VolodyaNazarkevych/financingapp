//
//  RootAssembly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import DITranquillity

final class RootAssembly {
    static var container: DIContainer {
        DISetting.Log.level = .verbose
        DISetting.Log.tab = " "
        DISetting.Log.fun = diLog

        let container = DIContainer()

        container.append([
            UIWindowAssembly.self,
            LaunchAssembly.self,
            NetworkAssembly.self
        ])

        #if DEBUG
        if !container.makeGraph().checkIsValid(checkGraphCycles: true) {
            fatalError("invalid graph")
        }
        #endif

        return container
    }

    static private func diLog(level: DILogLevel, msg: String) {
        switch level {
        case .none:
            break
        case .error:
            print("DI:Error: \(msg)")
        case .warning:
            print("DI:Warning: \(msg)")
        case .info:
            print("DI:Info: \(msg)")
        case .verbose:
            print("DI:Verbose: \(msg)")
        }
    }
}

extension DIContainer {
    /// Registers a collection of parts in the container.
    /// Registration means inclusion of all components indicated within.
    ///
    /// - Parameters:
    ///   - parts: the part type array
    @discardableResult
    public func append(_ parts: [DIPart.Type]) -> Self {
        parts.forEach { self.append(part: $0) }
        return self
    }
}
