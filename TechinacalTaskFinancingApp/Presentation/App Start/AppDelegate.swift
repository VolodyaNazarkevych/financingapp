//
//  AppDelegate.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 22.12.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    override init() {
        window = RootAssembly.container.resolve()
        super.init()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let launchViewController = RootAssembly.container.resolve() as LauncherViewController
        window?.rootViewController = launchViewController

        window?.makeKeyAndVisible()
        return true
    }
}
