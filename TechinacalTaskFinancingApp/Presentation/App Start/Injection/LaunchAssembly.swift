//
//  LaunchAssembly.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import Foundation
import DITranquillity

class LaunchAssembly: DIPart {
    static func load(container: DIContainer) {
        container.register(LauncherViewController.init)
            .as(check: LauncherViewController.self, { $0 })
    }
}
