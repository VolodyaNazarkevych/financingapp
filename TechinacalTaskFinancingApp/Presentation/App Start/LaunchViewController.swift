//
//  LaunchViewController.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 31.12.2020.
//

import UIKit

class LauncherViewController: UIViewController {

    static func makeLauncherViewController() -> LauncherViewController? {
        guard let vc = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateViewController(withIdentifier: "LauncherViewController") as? LauncherViewController else {
            return nil
        }
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        showMainFlow()
    }

    func showMainFlow() {
        let marketSummary = MarketSummaryAssembly.container.resolve() as MarketSummaryViewController
        let window = UIWindowAssembly.container.resolve() as UIWindow
        let navigation = UINavigationController(rootViewController: marketSummary)
        navigation.navigationBar.barTintColor = .black
        navigation.navigationBar.tintColor = .white
        navigation.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        window.rootViewController = navigation
    }
}
