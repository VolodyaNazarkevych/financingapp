//
//  StockSummaryViewModel.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation
import RxSwift
import RxCocoa

protocol StockSummaryViewModel: BaseViewModel {
    func setMarketSummary(_ market: MarketSummaryModel)
    var stockSummary: PublishSubject<StockSummaryModel> { get }
    var market: BehaviorSubject<MarketSummaryModel?> { get }

    func getStockSummary()
}

class StockSummaryViewModelImpl: StockSummaryViewModel {

    var loading: PublishSubject<Bool> = PublishSubject()
    var error: PublishSubject<String> = PublishSubject()
    var stockSummary: PublishSubject<StockSummaryModel> = PublishSubject()
    var market: BehaviorSubject<MarketSummaryModel?> = BehaviorSubject(value: nil)

    private let networkManager: YahooFinanceNetworkManager
    private let disposeBag = DisposeBag()

    init(networkManager: YahooFinanceNetworkManager) {
        self.networkManager = networkManager
    }

    func setMarketSummary(_ market: MarketSummaryModel) {
        self.market.onNext(market)
    }

    func getStockSummary() {
        guard let stockName = try? market.value()?.symbol else { return }
        loading.onNext(true)
        networkManager.getStockSummary(name: stockName) { [weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            switch result {
            case .success(let stockSummary):
                self.stockSummary.onNext(stockSummary)
            case .failure(let error):
                print("Error: \(error)")
                self.error.onNext("Something went wrong")
            }
        }
    }
}
