//
//  StockSummaryModel.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation

struct StockSummaryModel: Codable {
    let symbol: String
    let defaultKeyStatistics: DefaultKeyStatistics?
    let price: PriceModel?
    // TODO: determine object structure
    let summaryDetail: SummaryDetailModel?
    let summaryProfile: SummaryProfileModel?

    enum CodingKeys: String, CodingKey {
        case symbol
        case price
        case defaultKeyStatistics
        case summaryProfile
        case summaryDetail
    }
    // TODO: Parse another response values
}

struct PriceModel: Codable {
    let quoteSourceName: String?
    let regularMarketOpen: GeneralFormatModel?
    let averageDailyVolume3Month: GeneralFormatModel?
    let exchange: String?
    let regularMarketTime: TimeInterval
    let volume24Hr: GeneralFormatModel?
    let regularMarketDayHigh: GeneralFormatModel?
    let shortName: String?
    let longName: String?
    let averageDailyVolume10Day: GeneralFormatModel?
    let regularMarketChange: GeneralFormatModel?
    let currencySymbol: String?
    let regularMarketPreviousClose: GeneralFormatModel?
    let preMarketPrice: GeneralFormatModel?
    let exchangeDataDelayedBy: TimeInterval?
    let toCurrency: String?
    let postMarketChange: GeneralFormatModel?
    let postMarketPrice: GeneralFormatModel?
    let exchangeName: String?
    let preMarketChange: GeneralFormatModel?
    let circulatingSupply: GeneralFormatModel?
    let regularMarketDayLow: GeneralFormatModel?
    let priceHint: GeneralFormatModel?
    let currency: String?
    let regularMarketPrice: GeneralFormatModel?
    let regularMarketVolume: GeneralFormatModel?
    let lastMarket: GeneralFormatModel?
    let regularMarketSource: String?
    let openInterest: GeneralFormatModel?
    let marketState: String?
    let underlyingSymbol: String?
    let marketCap: GeneralFormatModel?
    let quoteType: String?
    let volumeAllCurrencies: GeneralFormatModel?
    let strikePrice: GeneralFormatModel?
    let symbol: String?
    let maxAge: Double?
    let fromCurrency: String?
    let regularMarketChangePercent: GeneralFormatModel?
}

struct SummaryDetailModel: Codable {
    let previousClose: GeneralFormatModel?
    let regularMarketOpen: GeneralFormatModel?
    let twoHundredDayAverage: GeneralFormatModel?
    let trailingAnnualDividendYield: GeneralFormatModel?
    let payoutRatio: GeneralFormatModel?
    let volume24Hr: GeneralFormatModel?
    let regularMarketDayHigh: GeneralFormatModel?
    let navPrice: GeneralFormatModel?
    let averageDailyVolume10Day: GeneralFormatModel?
    let totalAssets: GeneralFormatModel?
    let regularMarketPreviousClose: GeneralFormatModel?
    let fiftyDayAverage: GeneralFormatModel?
    let trailingAnnualDividendRate: GeneralFormatModel?
    let open: GeneralFormatModel?
    let toCurrency: GeneralFormatModel?
    let averageVolume10days: GeneralFormatModel?
    let expireDate: GeneralFormatModel?
    let yield: GeneralFormatModel?
    let algorithm: GeneralFormatModel?
    let dividendRate: GeneralFormatModel?
    let exDividendDate: GeneralFormatModel?
    let beta: GeneralFormatModel?
    let circulatingSupply: GeneralFormatModel?
    let startDate: GeneralFormatModel?
    let regularMarketDayLow: GeneralFormatModel?
    let priceHint: GeneralFormatModel?
    let currency: String?
    let regularMarketVolume: GeneralFormatModel?
    let lastMarket: GeneralFormatModel?
    let maxSupply: GeneralFormatModel?
    let openInterest: GeneralFormatModel?
    let marketCap: GeneralFormatModel?
    let volumeAllCurrencies: GeneralFormatModel?
    let strikePrice: GeneralFormatModel?
    let averageVolume: GeneralFormatModel?
    let priceToSalesTrailing12Months: GeneralFormatModel?
    let dayLow: GeneralFormatModel?
    let ask: GeneralFormatModel?
    let ytdReturn: GeneralFormatModel?
    let askSize: GeneralFormatModel?
    let volume: GeneralFormatModel?
    let fiftyTwoWeekHigh: GeneralFormatModel?
    let forwardPE: GeneralFormatModel?
    let maxAge: Double?
    let fromCurrency: String?
    let fiveYearAvgDividendYield: GeneralFormatModel?
    let fiftyTwoWeekLow: GeneralFormatModel?
    let bid: GeneralFormatModel?
    let tradeable: Bool?
    let dividendYield: GeneralFormatModel?
    let bidSize: GeneralFormatModel?
    let dayHigh: GeneralFormatModel?
}

struct SummaryProfileModel: Codable {
    let zip: String
    let sector: String
    let fullTimeEmployees: Int
    let longBusinessSummary: String
    let city: String
    let phone: String
    let country: String
    //let companyOfficers: [CompanyOfficerModel]
    let website: String
    let maxAge: Double
    let address1: String
    let industry: String
    let address2: String?

    enum CodingKeys: String, CodingKey {
        case zip, sector, fullTimeEmployees, longBusinessSummary, city, phone,
             country
//        case companyOfficers,
        case website, maxAge, address1, industry, address2
    }
}

//struct CompanyOfficerModel: Codable {
//    enum CodingKeys: String, CodingKey {}
//}

struct DefaultKeyStatistics: Codable {
    let annualHoldingsTurnover: GeneralFormatModel
    let enterpriseToRevenue: GeneralFormatModel
    let beta3Year: GeneralFormatModel
    let profitMargins: GeneralFormatModel
    let enterpriseToEbitda: GeneralFormatModel
    let fiftyTwoWeekChange: GeneralFormatModel
    let morningStarRiskRating: GeneralFormatModel
    let forwardEps: GeneralFormatModel
    let revenueQuarterlyGrowth: GeneralFormatModel
    let sharesOutstanding: GeneralFormatModel
    let fundInceptionDate: GeneralFormatModel
    let annualReportExpenseRatio: GeneralFormatModel
    let totalAssets: GeneralFormatModel
    let bookValue: GeneralFormatModel
    let sharesShort: GeneralFormatModel
    let sharesPercentSharesOut: GeneralFormatModel
    // Not determined for now
    //let fundFamily:
    let lastFiscalYearEnd: GeneralFormatModel
    let heldPercentInstitutions: GeneralFormatModel
    let netIncomeToCommon: GeneralFormatModel
    let trailingEps: GeneralFormatModel
    let lastDividendValue: GeneralFormatModel
    let sandP52WeekChange: GeneralFormatModel
    let priceToBook: GeneralFormatModel
    let heldPercentInsiders: GeneralFormatModel
    let nextFiscalYearEnd: GeneralFormatModel
    let yield: GeneralFormatModel
    let shortRatio: GeneralFormatModel
    let sharesShortPreviousMonthDate: GeneralFormatModel
    let floatShares: GeneralFormatModel
    let beta: GeneralFormatModel
    let enterpriseValue: GeneralFormatModel
    let priceHint: GeneralFormatModel
    let threeYearAverageReturn: GeneralFormatModel
    let lastSplitDate: GeneralFormatModel
    let lastSplitFactor: GeneralFormatModel
    let legalType: GeneralFormatModel
    let lastDividendDate: GeneralFormatModel
    let morningStarOverallRating: GeneralFormatModel
    let earningsQuarterlyGrowth: GeneralFormatModel
    let priceToSalesTrailing12Months: GeneralFormatModel
    let dateShortInterest: GeneralFormatModel
    let pegRatio: GeneralFormatModel
    let ytdReturn: GeneralFormatModel
    let forwardPE: GeneralFormatModel
    let maxAge: Double
    let lastCapGain: GeneralFormatModel
    let shortPercentOfFloat: GeneralFormatModel
    let sharesShortPriorMonth: GeneralFormatModel
    let impliedSharesOutstanding: GeneralFormatModel
    let fiveYearAverageReturn: GeneralFormatModel

    // Not determined for now
    // let category:

    enum CodingKeys: String, CodingKey {
        case annualHoldingsTurnover, enterpriseToRevenue, beta3Year, profitMargins, enterpriseToEbitda
        case fiftyTwoWeekChange = "52WeekChange"
        case morningStarRiskRating, forwardEps, revenueQuarterlyGrowth, sharesOutstanding, fundInceptionDate,
        annualReportExpenseRatio, totalAssets, bookValue, sharesShort, sharesPercentSharesOut,
        lastFiscalYearEnd, heldPercentInstitutions, netIncomeToCommon, trailingEps, lastDividendValue
        case sandP52WeekChange = "SandP52WeekChange"
        case priceToBook, heldPercentInsiders, nextFiscalYearEnd, yield, shortRatio, sharesShortPreviousMonthDate,
             floatShares, beta, enterpriseValue, priceHint, threeYearAverageReturn, lastSplitDate, lastSplitFactor, legalType,
             lastDividendDate, morningStarOverallRating, earningsQuarterlyGrowth, priceToSalesTrailing12Months,
             dateShortInterest, pegRatio, ytdReturn, forwardPE
        case maxAge
        case lastCapGain, shortPercentOfFloat, sharesShortPriorMonth, impliedSharesOutstanding, fiveYearAverageReturn
    }
}


