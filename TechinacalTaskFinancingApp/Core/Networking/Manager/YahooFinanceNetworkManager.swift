//
//  YahooFinanceNetworkManager.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation

enum NetworkResponse: Error {
    case success
    case authenticationError
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode

    var errorDescription: String {
        switch self {
        case .success: return ""
        case .authenticationError: return "You need to be authenticated first."
        case .badRequest: return "Bad request"
        case .outdated: return "The url you requested is outdated."
        case .failed: return "Network request failed."
        case .noData: return "Response returned with no data to decode."
        case .unableToDecode: return "We could not decode the response."
        }
    }
}

enum StatusCodeResult<String>{
    case success
    case failure(Error)
}

enum MarkerSummaryRequestError: Error {
    case customError
}

// TODO: Separate it for each separated Router
struct YahooFinanceNetworkManager {
    static let environment: NetworkEnvironment = .production
    //TODO: AUTH KEYS GOES HERE
    let router = Router<YahooFinanceApi>()

    // TODO: Could be simplified and refactored
    func getMarketSummary(completion: @escaping (Result<[MarketSummaryModel], Error>) -> Void) {
        router.request(.marketSummary) { data, response, error in

            if let error = error {
                return completion(.failure(error))
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(.failure(NetworkResponse.noData))
                        return
                    }
                    do {
                        print(responseData)
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        print(jsonData)
                        let apiResponse = try JSONDecoder().decode(MarketSummaryAndSparkResponseModel.self, from: responseData)

                        guard apiResponse.marketSummaryAndSparkResponse.error == nil else {
                            completion(.failure(MarkerSummaryRequestError.customError))
                            return
                        }

                        completion(.success(apiResponse.marketSummaryAndSparkResponse.result))
                    } catch {
                        print(error)
                        completion(.failure(NetworkResponse.unableToDecode))
                    }
                case .failure(let networkFailureError):
                    completion(.failure(networkFailureError))
                }
            }
        }
    }

    func getStockSummary(name: String, completion: @escaping (Result<StockSummaryModel, Error>) -> Void) {
        router.request(.stockSummary(name: name)) { data, response, error in

            if let error = error {
                return completion(.failure(error))
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(.failure(NetworkResponse.noData))
                        return
                    }
                    do {
                        print(responseData)
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        print(jsonData)
                        let apiResponse = try JSONDecoder().decode(StockSummaryModel.self, from: responseData)

                        completion(.success(apiResponse))
                    } catch {
                        print(error)
                        completion(.failure(NetworkResponse.unableToDecode))
                    }
                case .failure(let networkFailureError):
                    completion(.failure(networkFailureError))
                }
            }
        }
    }

    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> StatusCodeResult<String> {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError)
        case 501...599: return .failure(NetworkResponse.badRequest)
        case 600: return .failure(NetworkResponse.outdated)
        default: return .failure(NetworkResponse.failed)
        }
    }
}
