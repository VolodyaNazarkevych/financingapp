//
//  NetworkEnvironment.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation

enum NetworkEnvironment {
    case qa
    case production
    case staging
}
