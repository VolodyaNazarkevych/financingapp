//
//  MarketSummaryEndpoint.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation

public enum YahooFinanceApi {
    case marketSummary
    case stockSummary(name: String)
}

extension YahooFinanceApi: EndPointType {

    var environmentBaseURL : String {
        switch YahooFinanceNetworkManager.environment {
        case .production: return "https://apidojo-yahoo-finance-v1.p.rapidapi.com/"
        case .qa: return "https://apidojo-yahoo-finance-v1.p.rapidapi.com/"
        case .staging: return "https://apidojo-yahoo-finance-v1.p.rapidapi.com/"
        }
    }

    var authentificationHeaders: Dictionary<String, String> {[
        "x-rapidapi-key": "c1f56a405emshb3cbf12bd0223d9p1919ddjsn98977e497eca",
        "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com"
    ]}

    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }

    var path: String {
        switch self {
        case .marketSummary:
            return "market/v2/get-summary"
        case .stockSummary:
            return "stock/v2/get-summary"
        }
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        switch self {
        case .marketSummary:
            return .requestParametersAndHeaders(bodyParameters: nil,
                                                bodyEncoding: .urlEncoding,
                                                urlParameters: ["region": "US"],
                                                additionHeaders: authentificationHeaders)
        case .stockSummary(let name):
            return .requestParametersAndHeaders(bodyParameters: nil,
                                                bodyEncoding: .urlEncoding,
                                                urlParameters: ["symbol": name,
                                                                "region": "US"],
                                                additionHeaders: authentificationHeaders)
        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}


