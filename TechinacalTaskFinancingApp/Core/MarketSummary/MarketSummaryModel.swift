//
//  MarketSummary.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 22.12.2020.
//

import Foundation

struct MarketSummaryAndSparkResponseModel: Codable {
    let marketSummaryAndSparkResponse: MarketSummaryAndSparkModel

    enum CokingKeys: String, CodingKey {
        case marketSummaryAndSparkResponse
    }
}

struct MarketSummaryAndSparkModel: Codable {
    let result: [MarketSummaryModel]
    let error: ErrorModel?

    enum CodingKeys: String, CodingKey {
        case result, error
    }
}

struct MarketSummaryModel: Codable {
    let fullExchangeName: String
    let exchangeTimezoneName: String
    let symbol: String
    let gmtOffSetMilliseconds: Int
    let firstTradeDateMilliseconds: Int
    let exchangeDataDelayedBy: Double
    let language: String
    let regularMarketTime: GeneralFormatModel
    let exchangeTimezoneShortName: String
    // TODO: use enum (will require write custom decode, encode)
    let quoteType: String
    // TODO: use enum (will require write custom decode, encode)
    let marketState: String
    // TODO: Research regarding types and use enum (will require write custom decode, encode)
    let market: String
    let spark: Spark
    // CHECK:  Posibly Int?
    let priceHint: Double
    let tradeable: Bool
    // CHECK:  Posibly Int?
    let sourceInterval: TimeInterval
    let exchange: String
    let region: String
    let shortName: String?
    let regularMarketPreviousClose: GeneralFormatModel
    let triggerable: Bool

    enum CodingKeys: String, CodingKey {
        case fullExchangeName, exchangeTimezoneName, symbol, gmtOffSetMilliseconds, firstTradeDateMilliseconds,
             exchangeDataDelayedBy, language, regularMarketTime, exchangeTimezoneShortName, quoteType,
             marketState, market, spark, priceHint, tradeable, sourceInterval, exchange, region, shortName,
             regularMarketPreviousClose, triggerable
    }
}

struct GeneralFormatModel: Codable {
    let raw: Double?
    let fmt: String?
    let longFmt: String?

    enum CodingKeys: String, CodingKey {
        case raw, fmt, longFmt
    }
}

extension GeneralFormatModel {
    enum DataType {
        case date
        case price
        // TODO:
    }
    // TODO: or determine inside custom decoder/encoder method
    // TODO:
    var type: DataType {
        return .date
    }

}

struct Spark: Codable {
    let symbol: String

    // timestamps
    let end: TimeInterval?
    let start: TimeInterval?
    let timestamp: [TimeInterval]

    let dataGranularity: Double
    let close: [Double]?
    //let previousClose: Double
    //let chartPreviousClose: Double

    enum CodingKeys: String, CodingKey {
        case symbol, end, start, timestamp, dataGranularity, close
        //case close, previousClose, chartPreviousClose
    }
}

struct ErrorModel: Codable {
    let errIndex: Int
    let code: String
    let message: String
    let description: String

    enum CodingKeys: String, CodingKey {
        case errIndex, code, message, description
    }
}

