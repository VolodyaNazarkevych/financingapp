//
//  MarketSummaryViewModel.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 22.12.2020.
//

import Foundation
import RxSwift

protocol MarketSummaryViewModel: BaseViewModel {
    var marketSummaries: PublishSubject<[MarketSummaryModel]> { get }

    func getMarketSummary()
    func searchMarketSummary(for searchText: String)
    func startUpdatingMarketSummaryPeriodically()
}

class MarketSummaryViewModelImpl: MarketSummaryViewModel {

    private var marketSummariesValues: [MarketSummaryModel] = []
    var marketSummaries: PublishSubject<[MarketSummaryModel]> = PublishSubject()

    var loading: PublishSubject<Bool> = PublishSubject()
    var error: PublishSubject<String> = PublishSubject()

    private let networkManager: YahooFinanceNetworkManager
    private let disposable = DisposeBag()
    private let refreshTimeInterval: DispatchTimeInterval = .seconds(8)
    private var lastSearchText: String = ""

    init(networkManager: YahooFinanceNetworkManager) {
        self.networkManager = networkManager
    }

    func getMarketSummary() {
        getMarketSummary(shouldShowLoading: true)
    }

    func searchMarketSummary(for searchText: String) {
        self.lastSearchText = searchText
        guard !searchText.isEmpty else {
            marketSummaries.onNext(marketSummariesValues)
            return
        }

        let filtered = marketSummariesValues.filter { market in
            guard let name = market.shortName else { return false }
            return name.containsIgnoringCase(find: searchText)
        }
        marketSummaries.onNext(filtered)
    }

    func startUpdatingMarketSummaryPeriodically() {
        Observable<Int>.interval(refreshTimeInterval, scheduler: MainScheduler.instance)
            .debug("Refresh interval")
            .subscribe(onNext: { [weak self] _ in
                self?.getMarketSummary(shouldShowLoading: false)
            })
            .disposed(by: disposable)
    }

    private func updateMarketSummary(_ summaries: [MarketSummaryModel]) {
        marketSummariesValues = summaries

        // Update current state of search process
        searchMarketSummary(for: lastSearchText)
    }

    private func getMarketSummary(shouldShowLoading: Bool) {
        if shouldShowLoading { loading.onNext(true) }
        networkManager.getMarketSummary { [weak self, shouldShowLoading] result in
            guard let self = self else { return }
            if shouldShowLoading { self.loading.onNext(false) }
            switch result {
            case .success(let marketSummaries):
                self.updateMarketSummary(marketSummaries)
            case .failure(let error):
                print("Error: \(error)")
                self.error.onNext("Something went wrong")
            }
        }
    }
}
