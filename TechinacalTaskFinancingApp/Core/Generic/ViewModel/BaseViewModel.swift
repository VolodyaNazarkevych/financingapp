//
//  BaseViewModel.swift
//  TechinacalTaskFinancingApp
//
//  Created by Volodymyr Nazarkevych on 26.12.2020.
//

import Foundation
import RxSwift

// TODO: add supporting of it on Base View Controller implementation
protocol BaseViewModel {
    var loading: PublishSubject<Bool> { get }
    var error: PublishSubject<String> { get }
}
